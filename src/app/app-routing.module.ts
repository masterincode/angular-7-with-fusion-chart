import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OilReserveComponent } from './component/oil-reserve/oil-reserve.component';
import { MultiSeriesColumnComponent } from './component/multi-series-column/multi-series-column.component';

const routes: Routes = [
  {path:'oil' , component: OilReserveComponent},
  {path:'multi' , component: MultiSeriesColumnComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
