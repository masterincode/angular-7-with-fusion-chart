import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';

// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';

import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OilReserveComponent } from './component/oil-reserve/oil-reserve.component';
import { MultiSeriesColumnComponent } from './component/multi-series-column/multi-series-column.component';

@NgModule({
  declarations: [
    AppComponent,
    OilReserveComponent,
    MultiSeriesColumnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FusionChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
