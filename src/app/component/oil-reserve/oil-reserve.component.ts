import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-oil-reserve',
  templateUrl: './oil-reserve.component.html',
  styleUrls: ['./oil-reserve.component.css']
})
export class OilReserveComponent implements OnInit{
    constructor() { }
    
  title:string='Fusion Chart';
  dataSource: Object;

    ngOnInit(): void {
       
    console.log('----ngOnInit----');
        
    this.dataSource = {
        chart: {
             "showToolTip":true,
             "toolTipBgColor":"#f47a42",
            
            "caption": "Monthly revenue for last year",
            "subCaption": "Harry's SuperMart",
            "xAxisName": "Months",
            "yAxisName": "Revenues (In INR)",
            "theme": "fusion"
        },
        data: [
            { "label": "Jan", "value": "420000" },
            { "label": "Feb", "value": "810000" },
            { "label": "Mar", "value": "720000" },
            { "label": "Apr", "value": "550000" },
            { "label": "May", "value": "910000" },
            { "label": "Jun", "value": "510000" },
            { "label": "Jul", "value": "680000" }           
        ]
     }; // end of this.dataSource
    }
  
}
