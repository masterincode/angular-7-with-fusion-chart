import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OilReserveComponent } from './oil-reserve.component';

describe('OilReserveComponent', () => {
  let component: OilReserveComponent;
  let fixture: ComponentFixture<OilReserveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilReserveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OilReserveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
