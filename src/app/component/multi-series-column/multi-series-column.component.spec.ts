import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSeriesColumnComponent } from './multi-series-column.component';

describe('MultiSeriesColumnComponent', () => {
  let component: MultiSeriesColumnComponent;
  let fixture: ComponentFixture<MultiSeriesColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSeriesColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSeriesColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
