import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-series-column',
  templateUrl: './multi-series-column.component.html',
  styleUrls: ['./multi-series-column.component.css']
})
export class MultiSeriesColumnComponent implements OnInit {

    width = 700;
    height = 500;
  type = "mscolumn3d";
  dataFormat = "json";
  dataSource:Object;
 

  
  constructor() { }

  ngOnInit() {

    const data = {
        chart: {
          caption: "App Publishing Trend",
          subcaption: "2012-2016",
          xaxisname: "Years",
          yaxisname: "Total number of apps in store",
          // formatnumberscale: "1",
          theme: "fusion"
        },
        categories: [
          {
            category: [
              {
                label: "2012"
              },
              {
                label: "2013"
              },
              {
                label: "2014"
              },
              {
                label: "2015"
              },
              {
                label: "2016"
              }
            ]
          }
        ],
        dataset: [
          {
            seriesname: "iOS App Store",
            data: [
              {
                value: "125000"
              },
              {
                value: "300000"
              },
              {
                value: "480000"
              },
              {
                value: "800000"
              },
              {
                value: "1100000"
              }
            ]
          },
          {
            seriesname: "Google Play Store",
            data: [
              {
                value: "70000"
              },
              {
                value: "150000"
              },
              {
                value: "350000"
              },
              {
                value: "600000"
              },
              {
                value: "1400000"
              }
            ]
          }
         
        ]
      };

      this.dataSource = data;
      
  }

}
